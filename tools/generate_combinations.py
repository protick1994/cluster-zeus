



combinations = []


def generate_combinations(index, candidates, running_array = [], comb_len = 1, n = 10):
    if len(running_array) == comb_len:
        combinations.append(running_array.copy())
        return
    elif index >= n:
        return

    generate_combinations(index + 1, candidates, running_array, comb_len, n)

    running_array.append(candidates[index])

    generate_combinations(index + 1, candidates, running_array, comb_len, n)

    running_array.pop()

    return


def generate(n, comb_len):
    combinations.clear()
    candidates = [i for i in range(0, n)]

    generate_combinations(0, candidates, [], comb_len, n)

    combinations.sort()

    return combinations
    # return [combinations[0]]










