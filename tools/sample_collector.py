import json
from _collections import defaultdict


feature_map = defaultdict(list)
import random


def provide_top_p_samples(label, num_of_samples, normalized=False):
    json_key = "../tools/data/top_3000_sample_{}".format(label)
    json_file = json_key + ".json"
    f = open(json_file, )
    samples = json.load(f)
    selected_samples = []

    for i in range(0, num_of_samples):
        selected_sample = samples[i][1]
        if normalized:
            for index in range(0, len(selected_sample)):
                selected_sample[index] = selected_sample[index] / 255

        selected_samples.append(selected_sample)

    return selected_samples


def provide_random_p_samples(label, num_of_samples, normalized=False):
    json_key = "../tools/data/top_3000_sample_{}".format(label)
    json_file = json_key + ".json"
    f = open(json_file, )
    samples = json.load(f)
    selected_samples = random.sample(samples, num_of_samples)
    selected_samples = [x[1] for x in selected_samples]

    if normalized:
        for i in range(0, len(selected_samples)):
            for index in range(0, len(selected_samples[i])):
                selected_samples[i][index] = selected_samples[i][index] / 255

    return selected_samples







