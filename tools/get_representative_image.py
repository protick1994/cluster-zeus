
def calculate_average_image(memeber_lst, length):

    result_vector = [0] * length
    for member in memeber_lst:
        for index in range(0, length):
            result_vector[index] += member[index]

    result_vector = [x/len(memeber_lst) for x in result_vector]
    return result_vector
