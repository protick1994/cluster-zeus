import json
from _collections import defaultdict


feature_map = defaultdict(list)


def provide_top_p_samples(label, num_of_samples, normalized=False):
    json_key = "top_600_sample_{}".format(label)
    json_file = json_key + ".json"
    f = open(json_file, )
    samples = json.load(f)
    selected_samples = []

    for i in range(0, num_of_samples):
        selected_sample = samples[i][1]
        if normalized:
            for index in range(0, len(selected_sample)):
                selected_sample[index] = selected_sample[index] / 255

        selected_samples.append(selected_sample)

    return selected_samples







