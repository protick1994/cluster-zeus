# base_key format = ALGO-SAMPLE_SIZE-1_2_3_4-N_CLUSTER

def make_base_key(algo, sample_size, combination, n_clusers):
    digit_key = ""
    print(combination)
    for digit in combination:
        if len(digit_key) != 0:
            digit_key = digit_key + "_" + str(digit)
        else:
            digit_key = digit_key + str(digit)

    print(digit_key)

    return "{}-{}-{}-{}".format(algo, sample_size, digit_key, n_clusers)