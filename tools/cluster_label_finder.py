from collections import defaultdict


def infer_cluster_labels(generated_labels, actual_labels):
    ans_dict = {}
    d = defaultdict(list)

    for index in range(0, len(generated_labels)):
        cluster_label = generated_labels[index]
        actual_digit = actual_labels[index]
        if actual_digit != -1:
            d[cluster_label].append(actual_digit)

    for key in d:
        ans_dict[key] = max(d[key], key=d[key].count)

    return ans_dict






