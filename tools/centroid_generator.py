import csv
import json
from _collections import defaultdict
from queue import PriorityQueue

from scipy.spatial import distance

# 784 = 28 * 28
from tools.get_representative_image import calculate_average_image

feature_map = defaultdict(list)

import matplotlib.pyplot as plt
import numpy as np


# [(0, 980), (1, 1135), (2, 1032), (3, 1010), (4, 982), (5, 892), (6, 958), (7, 1028), (8, 974), (9, 1009)]

with open("mnist_train.csv", "r") as f:
    reader = csv.reader(f, delimiter=",")
    for i, line in enumerate(reader):
        if i == 0:
            continue
        label = int(line[0])
        feature_vector = [int(x) for x in line[1:]]
        feature_map[label].append(feature_vector)


actual_labels = []
average_image_map = {}


for label in range(0, 10):
    average_image_map[label] = calculate_average_image(feature_map[label], 28*28)



plt.gray()  # B/W Images
plt.figure(figsize=(10, 9))  # Adjusting figure size
# Displaying a grid of 3x3 images
# 10 * 5
for i in range(10):
    plt.subplot(5, 2, i + 1)
    image = np.array(average_image_map[i] )
    image = image.reshape(28, 28)
    plt.imshow(image)
plt.show()

a = 1



