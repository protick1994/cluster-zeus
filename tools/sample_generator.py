import csv
import json
from _collections import defaultdict
from queue import PriorityQueue

from scipy.spatial import distance

# 784 = 28 * 28
from tools.get_representative_image import calculate_average_image

feature_map = defaultdict(list)


# [(0, 980), (1, 1135), (2, 1032), (3, 1010), (4, 982), (5, 892), (6, 958), (7, 1028), (8, 974), (9, 1009)]

with open("mnist_train.csv", "r") as f:
    reader = csv.reader(f, delimiter=",")
    for i, line in enumerate(reader):
        if i == 0:
            continue
        label = int(line[0])
        feature_vector = [int(x) for x in line[1:]]
        feature_map[label].append(feature_vector)


actual_labels = []
average_image_map = {}
label_to_sample_map = defaultdict(list)

for label in range(0, 10):
    average_image_map[label] = calculate_average_image(feature_map[label], 28*28)



PER_DIGIT_SAMPLE = 3000

for label in range(0, 10):
    data_set = []
    heap = PriorityQueue()
    for candidate in feature_map[label]:
        chosen_point = candidate
        reference_point = average_image_map[label]
        dst = distance.euclidean(chosen_point, reference_point)
        heap.put((-dst, candidate))
        if heap.qsize() >= PER_DIGIT_SAMPLE:
            heap.get()

    while heap.qsize() > 0:
        dst, chosen_candidate = heap.get()
        data_set.append((-dst, chosen_candidate))

    data_set.reverse()

    out_file = "data/top_3000_sample_{}.json".format(label)

    with open(out_file, "w") as ouf:
        json.dump(data_set, fp=ouf, indent=2)
