Instructions

1) create a virtual environment
2) pip3 install -r requirements.txt

3) Sample generation:
python3 sample_generator.py

4) Find variance within different writing styles of digits:
python3 digit-variance-finder.py

5) Generate mislabel rate within digits:
python3 k-means-mislabel_rate.py

6) Run clustering algorithms on all 3 length combinations of digits and dump the results in final_results directory
python3 cluster-bot.py

7) Generate graphs from previous step
python3 final_results_reader.py

8) Run poisoning attack:
python3 single-linkage-poisoning.py

9) Run obfuscation attack:
python3 single-linkage-obfuscation.py


