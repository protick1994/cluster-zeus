import json
import time
from _collections import defaultdict

import numpy as np
from sklearn.cluster import AgglomerativeClustering, MiniBatchKMeans, KMeans

from tools.cluster_label_finder import infer_cluster_labels
from tools.generate_combinations import generate as gen_combinations
from tools.result_key_maker import make_base_key
from tools.sample_collector import provide_top_p_samples

CLUSTERING_ALGORITHMS = ['single-linkage', 'average-linkage', 'ward-linkage', 'mini-k-means', 'k-means', 'complete-linkage']
# CLUSTERING_ALGORITHMS = ['single-linkage']


def select_proper_model(algo, n_clusters):
    if algo == 'single-linkage':
        return AgglomerativeClustering(linkage='single', n_clusters=n_clusters)
    if algo == 'average-linkage':
        return AgglomerativeClustering(linkage='average', n_clusters=n_clusters)
    if algo == 'ward-linkage':
        return AgglomerativeClustering(linkage='ward', n_clusters=n_clusters)
    if algo == 'complete-linkage':
        return AgglomerativeClustering(linkage='complete', n_clusters=n_clusters)
    if algo == 'mini-k-means':
        return MiniBatchKMeans(n_clusters=n_clusters)
    if algo == 'k-means':
        return KMeans(n_clusters=n_clusters)





feature_map = {}

# Final

# ALLOWABLE_MIN_LEN, ALLOWABLE_MAX_LEN = 3, 10
#
# PER_DIGIT_SAMPLE_INIT, PER_DIGIT_SAMPLE_DELTA, PER_DIGIT_SAMPLE_ITERATION = 50, 50, 15

# Test

ALLOWABLE_MIN_LEN, ALLOWABLE_MAX_LEN = 3, 3

ALLOWABLE_COMBINATION_LENGTHS = [3, 4, 10]

PER_DIGIT_SAMPLE_INIT, PER_DIGIT_SAMPLE_DELTA, PER_DIGIT_SAMPLE_ITERATION = 50, 25, 20


per_label_accuracy_dict = {}


for algo in CLUSTERING_ALGORITHMS:
    SAMPLE_SIZE = PER_DIGIT_SAMPLE_INIT
    for iteration in range(1, PER_DIGIT_SAMPLE_ITERATION + 1):
        for combination_len in ALLOWABLE_COMBINATION_LENGTHS:
            digit_combinations = gen_combinations(10, combination_len)
            print(combination_len)
            print(digit_combinations)

            for combination in digit_combinations:
                samples = []
                actual_labels = []


                n_clusers = combination_len

                cluster_delta = 1

                while True:
                    print("Starting experimentation on algorithm: {}, sample size: {}, labels: {}, clusters: {}".format(algo, SAMPLE_SIZE, combination, n_clusers))
                    n_clusers = n_clusers + cluster_delta
                    cluster_delta += 1

                    if n_clusers >= 10:
                        break


        SAMPLE_SIZE = SAMPLE_SIZE + PER_DIGIT_SAMPLE_DELTA




