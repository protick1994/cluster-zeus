import json
import os

result_director = "results"
from collections import defaultdict
from sklearn import metrics


def get_files_from_path():
    file_name_Set = set()
    for file in os.listdir(result_director):
        file_name_Set.add(file)

    return file_name_Set


def evaluate_result_for_algo(algo):
    file_set = get_files_from_path()
    attribute_str_set = set()
    d = {}
    for file in file_set:
        if file.startswith(algo):
            delimited_attributes = file[len(algo) + 1:]
            last_pos = delimited_attributes.rfind("-")
            attributes_str = delimited_attributes[: last_pos]
            attribute_str_set.add(attributes_str)
            #SAMPLE_SIZE - COMB - N_CLUSTER

    for attribute_str in attribute_str_set:
        attributes = attribute_str.split("-")
        sample_size = attributes[0]
        combination_key = attributes[1]
        n_clusters = attributes[2]
        if combination_key not in d:
            d[combination_key] = set()
        d[combination_key].add((sample_size, n_clusters))

    for combination_key in d:
        size_cluster_tuple_list = [x for x in d[combination_key]]
        size_cluster_tuple_list.sort()

        size_cluster_result = []

        for (size, cluster) in size_cluster_tuple_list:
            # ALGO-SAMPLE_SIZE-1_2_3_4-N_CLUSTER
            reconstructed_key_prefix = algo + "-" + size + "-" + combination_key + "-" + cluster

            out_file = "results/{}-{}.json".format(reconstructed_key_prefix, 'predicted_labels')
            f = open(out_file, )
            predicted_labels = json.load(f)

            out_file = "results/{}-{}.json".format(reconstructed_key_prefix, 'actual_labels')
            f = open(out_file, )
            actual_labels = json.load(f)

            out_file = "results/{}-{}.json".format(reconstructed_key_prefix, 'time')
            f = open(out_file, )
            time = json.load(f)

            total_samples = len(predicted_labels)
            correctly_labelled = 0
            for index in range(0, total_samples):
                if predicted_labels[index] == actual_labels[index]:
                    correctly_labelled += 1

            accuracy = correctly_labelled / total_samples
            adjusted_rand_score = metrics.adjusted_rand_score(actual_labels, predicted_labels)
            adjusted_mutual_info_score = metrics.adjusted_mutual_info_score(actual_labels, predicted_labels)
            homogeneity_score = metrics.homogeneity_score(actual_labels, predicted_labels)
            completeness_score = metrics.completeness_score(actual_labels, predicted_labels)
            v_measure_score = metrics.v_measure_score(actual_labels, predicted_labels)


            res_d = {}

            res_d["accuracy"] = accuracy
            res_d["adjusted_rand_score"] = adjusted_rand_score
            res_d["adjusted_mutual_info_score"] = adjusted_mutual_info_score
            res_d["homogeneity_score"] = homogeneity_score
            res_d["completeness_score"] = completeness_score
            res_d["v_measure_score"] = v_measure_score

            size_cluster_result.append((size, cluster, res_d))

        import os
        path = "final_results/{}".format(algo)
        if not os.path.exists(path):
            os.makedirs(path)

        out_file = "{}/{}.json".format(path, combination_key)

        with open(out_file, "w") as ouf:
            json.dump(size_cluster_result, fp=ouf, indent=2)






def evaluate_result():
    CLUSTERING_ALGORITHMS = ['single-linkage', 'average-linkage', 'ward-linkage', 'mini-k-means', 'k-means',
                             'complete-linkage']
    evaluate_result_for_algo(CLUSTERING_ALGORITHMS[0])


evaluate_result()





