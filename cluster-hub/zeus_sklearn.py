import csv
import random
from _collections import defaultdict
from scipy.spatial import distance

import numpy as np
from sklearn.cluster import AgglomerativeClustering, MiniBatchKMeans
import time
import matplotlib.pyplot as plt

from tools.get_representative_image import calculate_average_image
from scipy.spatial import distance
from queue import PriorityQueue


feature_map = defaultdict(list)


# [(0, 980), (1, 1135), (2, 1032), (3, 1010), (4, 982), (5, 892), (6, 958), (7, 1028), (8, 974), (9, 1009)]

with open("mnist_train.csv", "r") as f:
    reader = csv.reader(f, delimiter=",")
    for i, line in enumerate(reader):
        if i == 0:
            continue
        label = int(line[0])
        feature_vector = [int(x)/255 for x in line[1:]]
        feature_map[label].append(feature_vector)

data_set = []
actual_labels = []
average_image_map = {}

for label in range(0, 10):
    average_image_map[label] = calculate_average_image(feature_map[label], 28*28)



PER_DIGIT_SAMPLE = 200
TOTAL_SAMPLE = PER_DIGIT_SAMPLE * 10



for label in [0, 1, 8]:
    actual_labels = actual_labels + [label] * PER_DIGIT_SAMPLE
    heap = PriorityQueue()
    for candidate in feature_map[label]:
        chosen_point = candidate
        reference_point = average_image_map[label]
        dst = distance.euclidean(chosen_point, reference_point)
        heap.put((-dst, candidate))
        if heap.qsize() >= PER_DIGIT_SAMPLE:
            heap.get()

    while heap.qsize() > 0:
        chosen_candidate = heap.get()[1]
        data_set.append(chosen_candidate)


    # for count in range(0, 40):
    #     data_set.append(feature_map[label][random.randint(0, len(feature_map[label]) - 1)])
    #     chosen_point = feature_map[label][random.randint(0, len(feature_map[label]) - 1)]
    #     reference_point = average_image_map[label]
    #
    #     dst = distance.euclidean(chosen_point, reference_point)




np_array = np.array(data_set)

# plt.imshow(np_array[0].reshape(28, 28))
# plt.show()
#
# a = 2


linkage = 'single'
n_clusters = 3


t0 = time.time()


model = AgglomerativeClustering(linkage=linkage, n_clusters=n_clusters)
model.fit(np_array)







elapsed_time = time.time() - t0

for i in range(len(model.labels_)):
    print(model.labels_[i], actual_labels[i])









