import numpy as np
from sklearn.cluster import AgglomerativeClustering, MiniBatchKMeans, KMeans

from tools.cluster_label_finder import infer_cluster_labels
from tools.sample_collector import provide_top_p_samples, provide_random_p_samples

CLUSTERING_ALGORITHMS = ['single-linkage']


def calculate_centroid(memeber_lst, length):
    result_vector = [0] * length
    for member in memeber_lst:
        for index in range(0, length):
            result_vector[index] += member[index]

    result_vector = [x / len(memeber_lst) for x in result_vector]
    return result_vector


def get_tuned_attack_samples(centroid, attack_samples, allowed_perturbation_percentage):
    new_attack_samples = []
    for sample in attack_samples:
        new_attack_sample = [0] * (28*28)
        for index in range(0, 28*28):
            delta = centroid[index] - sample[index]
            new_attack_sample[index] = sample[index] + delta * (allowed_perturbation_percentage/100)
        new_attack_samples.append(new_attack_sample)

    return new_attack_samples


def select_proper_model(algo, n_clusters):
    if algo == 'single-linkage':
        return AgglomerativeClustering(linkage='single', n_clusters=n_clusters)
    if algo == 'average-linkage':
        return AgglomerativeClustering(linkage='average', n_clusters=n_clusters)
    if algo == 'ward-linkage':
        return AgglomerativeClustering(linkage='ward', n_clusters=n_clusters)
    if algo == 'complete-linkage':
        return AgglomerativeClustering(linkage='complete', n_clusters=n_clusters)
    if algo == 'mini-k-means':
        return MiniBatchKMeans(n_clusters=n_clusters)
    if algo == 'k-means':
        return KMeans(n_clusters=n_clusters)


def generate_clustering_assignment_matrix(cluster_labels):
    mat = [[0 for x in range(len(cluster_labels))] for y in range(len(cluster_labels))]

    for h in range(0, len(cluster_labels)):
        for w in range(0, len(cluster_labels)):
            if cluster_labels[h] == cluster_labels[w]:
                mat[h][w] = 1

    return mat


def diff_with_initial_assignment(actual_sample_size, mat, new_clusters):
    l = len(mat)

    new_mat = [[0 for x in range(l)] for y in range(l)]

    for h in range(0, l):
        for w in range(0, l):
            if new_clusters[h] == new_clusters[w]:
                new_mat[h][w] = 1

    dif = 0
    for i in range(0, l):
        for j in range(0, l):
            if mat[i][j] != new_mat[i][j]:
                dif += 1

    return dif


def adjustment_with_cluster_number(algo, samples, actual_sample_size, n_clusers=None, mat=None):
    try:
        np_array = np.array(samples)
        model = select_proper_model(algo=algo, n_clusters=n_clusers)
        model.fit(np_array)

        if mat:
            return diff_with_initial_assignment(actual_sample_size, mat, model.labels_)
        else:
            return model
    except Exception as e:
        a = 1


def rebuild_model_with_adjustment(algo, samples, actual_sample_size, actual_labels, mat=None):
    n_clusers = 4

    cluster_delta = 1

    if mat:
        ref_diff, optimal_cluster = -1, -1

        while True:

            diff = adjustment_with_cluster_number(algo=algo, samples=samples, actual_sample_size=actual_sample_size,
                                                  n_clusers=n_clusers, mat=mat)
            if ref_diff == -1:
                ref_diff = diff
                optimal_cluster = n_clusers
            elif diff < ref_diff:
                optimal_cluster = n_clusers
                ref_diff = diff

            n_clusers = n_clusers + 1

            if n_clusers > 100:
                break

        model = adjustment_with_cluster_number(algo=algo, samples=samples, actual_sample_size=actual_sample_size,
                                               n_clusers=optimal_cluster, mat=None)

        return model

    else:
        model = None
        while True:
            model = adjustment_with_cluster_number(algo=algo, samples=samples, actual_sample_size=actual_sample_size,
                                                   n_clusers=n_clusers, mat=None)

            cluster_label_to_digit_dict = infer_cluster_labels(generated_labels=model.labels_,
                                                               actual_labels=actual_labels)
            predicted_labels = []
            for sample_index in range(0, len(model.labels_)):
                predicted_labels.append(cluster_label_to_digit_dict[model.labels_[sample_index]])

            correct_labels = 0
            for i in range(0, len(actual_labels)):
                if actual_labels[i] == predicted_labels[i]:
                    correct_labels += 1

            accuracy = correct_labels / len(predicted_labels)

            if accuracy > .98:
                break

            n_clusers = n_clusers + 1

            if n_clusers > 100:
                break

        return model


algo = 'single-linkage'

feature_map = {}

MAX_ALLOWED_CLUSTERS = 100

SAMPLE_SIZE = 200

ATTACK_SAMPLE_SIZE = 50

digit_combinations = [[0, 1, 6]]
attack_digit = 3


import matplotlib.pyplot as plt


for combination in digit_combinations:
    print("Working with combination {}".format(combination))
    mat = None
    cluster_size_list = []

    # INIT three
    target_samples = []
    samples = []
    actual_labels = []
    for digit in combination:
        digit_samples = provide_random_p_samples(label=digit, num_of_samples=SAMPLE_SIZE, normalized=True)
        samples = samples + digit_samples
        if digit == 1:
            target_samples = target_samples + digit_samples.copy()
        actual_labels = actual_labels + [digit] * SAMPLE_SIZE

    # real sample size INIT
    actual_sample_size = len(samples)

    target_centroid = calculate_centroid(target_samples, 28*28)

    # Mutate
    # attack_samples = samples.copy()
    # label_of_attack_samples = actual_labels.copy()

    attack_samples = provide_random_p_samples(label=attack_digit, num_of_samples=ATTACK_SAMPLE_SIZE, normalized=True)
    label_of_attack_samples = [attack_digit] * ATTACK_SAMPLE_SIZE

    _model_adjusted = rebuild_model_with_adjustment(algo=algo, samples=samples + attack_samples,
                                                    actual_sample_size=actual_sample_size,
                                                    actual_labels=actual_labels + label_of_attack_samples)

    cluster_size_list = []
    # cluster_size_list.append((sample_input, _model_adjusted.n_clusters))
    mat = generate_clustering_assignment_matrix(_model_adjusted.labels_[0: actual_sample_size])
    original_cluster_labels = _model_adjusted.labels_.copy()
    original_cluster_number = _model_adjusted.n_clusters
    print("old clusters {}".format(_model_adjusted.n_clusters))

    show_digits_list = []
    for allowed_perturbation_percentage in range(2, 16, 1):
        print("perturbation  percentage {}".format(allowed_perturbation_percentage))

        tuned_attack_samples = get_tuned_attack_samples(target_centroid, attack_samples, allowed_perturbation_percentage)

        show_digits_list.append(tuned_attack_samples[0])




        _model_adjusted = rebuild_model_with_adjustment(algo=algo, samples=samples + tuned_attack_samples,
                                                        actual_sample_size=actual_sample_size,
                                                        actual_labels=actual_labels + label_of_attack_samples, mat=mat)

        cluster_label_to_digit_dict = infer_cluster_labels(generated_labels=_model_adjusted.labels_,
                                                           actual_labels=actual_labels + label_of_attack_samples)

        predicted_labels = []
        for sample_index in range(0, len(_model_adjusted.labels_)):
            predicted_labels.append(cluster_label_to_digit_dict[_model_adjusted.labels_[sample_index]])




        miss_classified_three = 0
        for index in range(actual_sample_size, actual_sample_size + len(attack_samples)):
            if predicted_labels[index] == 1:
                miss_classified_three += 1

        print("Miss classified three: {}".format((miss_classified_three/ATTACK_SAMPLE_SIZE)*100))


