import numpy as np

def infer_cluster_labels(model, actual_labels):
    inferred_labels = {}


    for i in range(model.n_clusters):

        # find index of points in cluster
        labels = []
        index = np.where(model.labels_ == i)

        # append actual labels for each point in cluster
        labels.append(actual_labels[index])

        # determine most common label
        if len(labels[0]) == 1:
            counts = np.bincount(labels[0])
        else:
            counts = np.bincount(np.squeeze(labels))

        # assign the cluster to a value in the inferred_labels dictionary
        if np.argmax(counts) in inferred_labels:
            # append the new number to the existing array at this slot
            inferred_labels[np.argmax(counts)].append(i)
        else:
            # create a new array in this slot
            inferred_labels[np.argmax(counts)] = [i]

    return inferred_labels



# test the infer_cluster_labels() and infer_data_labels() functions

# cluster_labels = infer_cluster_labels(kmeans, Y)
# X_clusters = kmeans.predict(X)
# predicted_labels = infer_data_labels(X_clusters, cluster_labels)
# print
# predicted_labels[:20]
# print
# Y[:20]


