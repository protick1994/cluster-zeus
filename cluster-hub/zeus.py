import csv
from _collections import defaultdict
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
import numpy as np
import random

# 784 = 28 * 28

feature_map = defaultdict(list)

# [(0, 980), (1, 1135), (2, 1032), (3, 1010), (4, 982), (5, 892), (6, 958), (7, 1028), (8, 974), (9, 1009)]

with open("mnist_test.csv", "r") as f:
    reader = csv.reader(f, delimiter=",")
    for i, line in enumerate(reader):
        if i == 0:
            continue
        label = int(line[0])
        feature_vector = [int(x) / 255 for x in line[1:]]
        feature_map[label].append(feature_vector)


data_set = []
for label in range(0, 10):
    for count in range(0, 20):
        data_set.append(feature_map[label][random.randint(0, len(feature_map[label]) - 1)])

np_array = np.array(data_set)


Z = linkage(y=np_array, method='single', metric='euclidean')

for element in Z:
    a = int(element[0])
    b = int(element[1])
    if a < len(data_set) and b < len(data_set):
        print(a//20, b//20)









