import socket,ssl,sys,pprint

hostname=sys.argv[1]

# hostname="google.com"
port=443

cadir="/etc/ssl/certs"

context=ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
context.load_verify_locations(capath=cadir)
context.verify_mode=ssl.CERT_REQUIRED
context.check_hostname=True

# context.check_hostname=True
# context.verify_mode=ssl.CERT_NONE


sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock.connect((hostname,port))

input("AftermakingTCPconnection.Pressanykeytocontinue...")

ssock=context.wrap_socket(sock,server_hostname=hostname, do_handshake_on_connect=False)

ssock.do_handshake()

pprint.pprint(ssock.getpeercert())

input("Afterhandshake.Pressanykeytocontinue...")


pprint.pprint(ssock.cipher())

ssock.shutdown(socket.SHUT_RDWR)
ssock.close()



