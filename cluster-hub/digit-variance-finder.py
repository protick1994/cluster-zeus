import json
import time
from _collections import defaultdict

import numpy as np
from sklearn import metrics
from sklearn.cluster import AgglomerativeClustering, MiniBatchKMeans, KMeans
import matplotlib.pyplot as plt

from tools.cluster_label_finder import infer_cluster_labels
from tools.generate_combinations import generate as gen_combinations
from tools.get_representative_image import calculate_average_image
from tools.result_key_maker import make_base_key
from tools.sample_collector import provide_top_p_samples, provide_random_p_samples

CLUSTERING_ALGORITHMS = ['single-linkage', 'average-linkage', 'k-means', 'complete-linkage']
# CLUSTERING_ALGORITHMS = ['single-linkage']


def select_proper_model(algo, n_clusters):
    if algo == 'single-linkage':
        return AgglomerativeClustering(linkage='single', n_clusters=n_clusters)
    if algo == 'average-linkage':
        return AgglomerativeClustering(linkage='average', n_clusters=n_clusters)
    if algo == 'ward-linkage':
        return AgglomerativeClustering(linkage='ward', n_clusters=n_clusters)
    if algo == 'complete-linkage':
        return AgglomerativeClustering(linkage='complete', n_clusters=n_clusters)
    if algo == 'mini-k-means':
        return MiniBatchKMeans(n_clusters=n_clusters)
    if algo == 'k-means':
        return KMeans(n_clusters=n_clusters)


def dump_results(algo, actual_labels, predicted_labels, elapsed_time, base_key):
    total_samples = len(predicted_labels)
    correctly_labelled = 0
    for index in range(0, total_samples):
        if predicted_labels[index] == actual_labels[index]:
            correctly_labelled += 1

    accuracy = correctly_labelled / total_samples
    adjusted_rand_score = metrics.adjusted_rand_score(actual_labels, predicted_labels)
    adjusted_mutual_info_score = metrics.adjusted_mutual_info_score(actual_labels, predicted_labels)
    homogeneity_score = metrics.homogeneity_score(actual_labels, predicted_labels)
    completeness_score = metrics.completeness_score(actual_labels, predicted_labels)
    v_measure_score = metrics.v_measure_score(actual_labels, predicted_labels)

    res_d = {}

    res_d["accuracy"] = accuracy
    res_d["adjusted_rand_score"] = adjusted_rand_score
    res_d["adjusted_mutual_info_score"] = adjusted_mutual_info_score
    res_d["homogeneity_score"] = homogeneity_score
    res_d["completeness_score"] = completeness_score
    res_d["v_measure_score"] = v_measure_score
    res_d["execution_time"] = elapsed_time

    import os
    path = "final_results/{}".format(algo)
    if not os.path.exists(path):
        os.makedirs(path)

    out_file = "{}/{}.json".format(path, base_key)

    with open(out_file, "w") as ouf:
        json.dump(res_d, fp=ouf, indent=2)






feature_map = {}

# Final

# ALLOWABLE_MIN_LEN, ALLOWABLE_MAX_LEN = 3, 10
#
# PER_DIGIT_SAMPLE_INIT, PER_DIGIT_SAMPLE_DELTA, PER_DIGIT_SAMPLE_ITERATION = 50, 50, 15

# Test

ALLOWABLE_MIN_LEN, ALLOWABLE_MAX_LEN = 3, 3

ALLOWABLE_COMBINATION_LENGTHS = [3, 10]

PER_DIGIT_SAMPLE_INIT, PER_DIGIT_SAMPLE_DELTA, PER_DIGIT_SAMPLE_ITERATION = 50, 50, 10

#TODO
MAX_ALLOWED_CLUSTERS = 100


per_label_accuracy_dict = {}

arr = []
for algo in ['single-linkage']:
    SAMPLE_SIZE = 500
    combs = [[x] for x in range(0, 10)]
    for combination in combs:
        samples = []
        actual_labels = []
        for digit in combination:
            samples = samples + provide_random_p_samples(label=digit, num_of_samples=SAMPLE_SIZE, normalized=False)
            actual_labels = actual_labels + [digit] * SAMPLE_SIZE

        np_array = np.array(samples)

        n_clusers = 10

        model = select_proper_model(algo=algo, n_clusters=n_clusers)
        model.fit(np_array)

        labels = model.labels_
        d = defaultdict(list)

        for index in range(len(labels)):
            d[labels[index]].append(samples[index])

        centroid_map = {}
        for key in d:
            centroid_map[key] = calculate_average_image(d[key], 28*28)



        plt.gray()  # B/W Images
        plt.figure(figsize=(10, 9))  # Adjusting figure size
        # Displaying a grid of 3x3 images
        # 10 * 5
        for i in range(10):
            plt.subplot(5, 2, i + 1)
            image = np.array(centroid_map[i])
            image = image.reshape(28, 28)
            plt.imshow(image)
        plt.savefig('k_res/{}.png'.format(combination[0]))
        plt.clf()
        var = np.var([centroid_map[key] for key in centroid_map])
        arr.append((var, combination[0]))

arr.sort()
print(arr)



