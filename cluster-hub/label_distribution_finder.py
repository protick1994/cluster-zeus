
# mnist_test.csv
# [(0, 980), (1, 1135), (2, 1032), (3, 1010), (4, 982), (5, 892), (6, 958), (7, 1028), (8, 974), (9, 1009)]

def find_dist(feature_map):
    label_info = []
    for label in feature_map:
        label_info.append((label, len(feature_map[label])))

    label_info.sort()
    print(label_info)