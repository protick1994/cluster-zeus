import json
import time
from _collections import defaultdict

import numpy as np
from sklearn import metrics
from sklearn.cluster import AgglomerativeClustering, MiniBatchKMeans, KMeans

from tools.cluster_label_finder import infer_cluster_labels
from tools.generate_combinations import generate as gen_combinations
from tools.result_key_maker import make_base_key
from tools.sample_collector import provide_top_p_samples

CLUSTERING_ALGORITHMS = ['single-linkage', 'average-linkage', 'k-means', 'complete-linkage']
# CLUSTERING_ALGORITHMS = ['single-linkage']


def select_proper_model(algo, n_clusters):
    if algo == 'single-linkage':
        return AgglomerativeClustering(linkage='single', n_clusters=n_clusters)
    if algo == 'average-linkage':
        return AgglomerativeClustering(linkage='average', n_clusters=n_clusters)
    if algo == 'ward-linkage':
        return AgglomerativeClustering(linkage='ward', n_clusters=n_clusters)
    if algo == 'complete-linkage':
        return AgglomerativeClustering(linkage='complete', n_clusters=n_clusters)
    if algo == 'mini-k-means':
        return MiniBatchKMeans(n_clusters=n_clusters)
    if algo == 'k-means':
        return KMeans(n_clusters=n_clusters)


def dump_results(algo, actual_labels, predicted_labels, elapsed_time, base_key):
    total_samples = len(predicted_labels)
    correctly_labelled = 0
    for index in range(0, total_samples):
        if predicted_labels[index] == actual_labels[index]:
            correctly_labelled += 1

    accuracy = correctly_labelled / total_samples
    adjusted_rand_score = metrics.adjusted_rand_score(actual_labels, predicted_labels)
    adjusted_mutual_info_score = metrics.adjusted_mutual_info_score(actual_labels, predicted_labels)
    homogeneity_score = metrics.homogeneity_score(actual_labels, predicted_labels)
    completeness_score = metrics.completeness_score(actual_labels, predicted_labels)
    v_measure_score = metrics.v_measure_score(actual_labels, predicted_labels)

    res_d = {}

    res_d["accuracy"] = accuracy
    res_d["adjusted_rand_score"] = adjusted_rand_score
    res_d["adjusted_mutual_info_score"] = adjusted_mutual_info_score
    res_d["homogeneity_score"] = homogeneity_score
    res_d["completeness_score"] = completeness_score
    res_d["v_measure_score"] = v_measure_score
    res_d["execution_time"] = elapsed_time

    import os
    path = "final_results/{}".format(algo)
    if not os.path.exists(path):
        os.makedirs(path)

    out_file = "{}/{}.json".format(path, base_key)

    with open(out_file, "w") as ouf:
        json.dump(res_d, fp=ouf, indent=2)






feature_map = {}

# Final

# ALLOWABLE_MIN_LEN, ALLOWABLE_MAX_LEN = 3, 10
#
# PER_DIGIT_SAMPLE_INIT, PER_DIGIT_SAMPLE_DELTA, PER_DIGIT_SAMPLE_ITERATION = 50, 50, 15

# Test

ALLOWABLE_MIN_LEN, ALLOWABLE_MAX_LEN = 3, 3

ALLOWABLE_COMBINATION_LENGTHS = [3, 10]

PER_DIGIT_SAMPLE_INIT, PER_DIGIT_SAMPLE_DELTA, PER_DIGIT_SAMPLE_ITERATION = 50, 50, 10

#TODO
MAX_ALLOWED_CLUSTERS = 100


per_label_accuracy_dict = {}


for algo in CLUSTERING_ALGORITHMS:
    SAMPLE_SIZE = PER_DIGIT_SAMPLE_INIT
    for iteration in range(1, PER_DIGIT_SAMPLE_ITERATION + 1):
        for combination_len in ALLOWABLE_COMBINATION_LENGTHS:
            digit_combinations = gen_combinations(10, combination_len)

            for combination in digit_combinations:
                samples = []
                actual_labels = []
                for digit in combination:
                    samples = samples + provide_top_p_samples(label=digit, num_of_samples=SAMPLE_SIZE, normalized=True)
                    actual_labels = actual_labels + [digit] * SAMPLE_SIZE

                np_array = np.array(samples)

                n_clusers = combination_len

                cluster_delta = 1

                while True:
                    print("Starting experimentation on algorithm: {}, sample size: {}, labels: {}, clusters: {}".format(algo, SAMPLE_SIZE, combination, n_clusers))
                    t0 = time.time()
                    model = select_proper_model(algo=algo, n_clusters=n_clusers)
                    model.fit(np_array)
                    # base_key format = ALGO-SAMPLE_SIZE-1_2_3_4-N_CLUSTER
                    base_key = make_base_key(algo, SAMPLE_SIZE, combination, n_clusers)

                    cluster_label_to_digit_dict = infer_cluster_labels(generated_labels=model.labels_,
                                                                       actual_labels=actual_labels)
                    predicted_labels = []
                    for sample_index in range(0, len(model.labels_)):
                        predicted_labels.append(cluster_label_to_digit_dict[model.labels_[sample_index]])

                    elapsed_time = time.time() - t0

                    dump_results(algo=algo, actual_labels=actual_labels, predicted_labels=predicted_labels,
                                 elapsed_time=elapsed_time, base_key=base_key)

                    correct_labels = 0
                    for i in range(0, len(actual_labels)):
                        if actual_labels[i] == predicted_labels[i]:
                            correct_labels += 1

                    accuracy = correct_labels / len(predicted_labels)

                    per_label_accuracy_dict[base_key] = accuracy

                    print("End of experimentation, accuracy {} ".format(accuracy))

                    n_clusers = n_clusers + cluster_delta
                    cluster_delta += 1

                    if n_clusers >= SAMPLE_SIZE or accuracy >= 1.0 or n_clusers > MAX_ALLOWED_CLUSTERS:
                        break

        SAMPLE_SIZE = SAMPLE_SIZE + PER_DIGIT_SAMPLE_DELTA



for key in per_label_accuracy_dict:
    print("key: {}, --> {}".format(key, per_label_accuracy_dict[key]))
