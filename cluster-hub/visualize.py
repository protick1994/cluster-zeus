from keras.datasets import mnist
import matplotlib.pyplot as plt


(x_train, y_train), (x_test, y_test) = mnist.load_data()

print("Training Data: {}".format(x_train.shape))
print("Training Labels: {}".format(y_train.shape))



print("Testing Data: {}".format(x_test.shape))
print("Testing Labels: {}".format(y_test.shape))



# EDA

fig, axs = plt.subplots(3, 3, figsize = (12, 12))
plt.gray()

for i, ax in enumerate(axs.flat):
    ax.matshow(x_train[i])
    ax.axis("off")
    ax.set_title("Number {}".format(y_train[i]))

fig.show()