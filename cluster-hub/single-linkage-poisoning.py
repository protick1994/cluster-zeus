import numpy as np
from sklearn.cluster import AgglomerativeClustering, MiniBatchKMeans, KMeans

from tools.cluster_label_finder import infer_cluster_labels
from tools.sample_collector import provide_top_p_samples, provide_random_p_samples

CLUSTERING_ALGORITHMS = ['single-linkage']


def get_predicted_labels(generated_labels, ac_labels):
    cluster_label_to_digit_dict = infer_cluster_labels(generated_labels=generated_labels,
                                                       actual_labels=ac_labels)
    predicted_labels = []
    for sample_index in range(0, len(ac_labels)):
        predicted_labels.append(cluster_label_to_digit_dict[generated_labels[sample_index]])

    return predicted_labels, cluster_label_to_digit_dict


def calculate_centroid(memeber_lst, length):
    result_vector = [0] * length
    for member in memeber_lst:
        for index in range(0, length):
            result_vector[index] += member[index]

    result_vector = [x / len(memeber_lst) for x in result_vector]
    return result_vector


def make_adverserial_sample_in_bridge(x, y):
    result_point = [0] * len(x)
    for index in range(0, len(x)):
        result_point[index] = (x[index] + y[index]) / 2
    return result_point


def get_dist(x, y):
    from scipy.spatial import distance
    return distance.euclidean(x, y)


def get_min_dist(set1, set2):
    from scipy.spatial import distance
    set1 = np.array(set1)
    set2 = np.array(set2)

    min_dist = -1
    p1, p2 = None, None

    for x in set1:
        for y in set2:
            d = distance.euclidean(x, y)

            if min_dist == -1:
                min_dist = d
                p1, p2 = x, y
            elif d < min_dist:
                min_dist = d
                p1, p2 = x, y

    return (min_dist, p1, p2)


def get_adverserial_sample(model, total_samples, actual_label_of_samples):
    cluster_to_samples = {}
    index = 0
    for cluster in model.labels_:
        if cluster not in cluster_to_samples:
            cluster_to_samples[cluster] = list()
        cluster_to_samples[cluster].append(total_samples[index])
        index += 1

    predicted_labels, cluster_label_to_digit_dict = get_predicted_labels(model.labels_,
                                                                         ac_labels=actual_label_of_samples)

    cluster_to_centroid = {}

    for cluster in cluster_to_samples:
        c = calculate_centroid(cluster_to_samples[cluster], 28 * 28)
        cluster_to_centroid[cluster] = c

    min_dist = -1
    min_dist_pair = None

    cluster_list = list(cluster_to_centroid.keys())
    for x in range(0, len(cluster_list)):
        for y in range(x + 1, len(cluster_list)):
            if cluster_list[x] == cluster_list[y]:
                continue

            c1 = cluster_to_centroid[cluster_list[x]]
            c2 = cluster_to_centroid[cluster_list[y]]

            dist, p1, p2 = get_min_dist(cluster_to_samples[cluster_list[x]], cluster_to_samples[cluster_list[y]])

            if min_dist == -1:
                min_dist = dist
                min_dist_pair = (p1, p2)
            elif dist < min_dist:
                min_dist = dist
                min_dist_pair = (p1, p2)

    return make_adverserial_sample_in_bridge(min_dist_pair[0], min_dist_pair[1])


def get_split_merge(actual_len, original_cluster_labels, new_clusters, prev_cluster_number, new_cluster_number):
    h = prev_cluster_number
    w = new_cluster_number

    k_mat = [[0 for x in range(w)] for y in range(h)]

    for index in range(0, actual_len):
        k_mat[original_cluster_labels[index]][new_clusters[index]] = 1

    temp = 0
    for i in range(0, h):
        for j in range(0, w):
            temp += k_mat[i][j]

    return (temp / h, temp / w)


def select_proper_model(algo, n_clusters):
    if algo == 'single-linkage':
        return AgglomerativeClustering(linkage='single', n_clusters=n_clusters)
    if algo == 'average-linkage':
        return AgglomerativeClustering(linkage='average', n_clusters=n_clusters)
    if algo == 'ward-linkage':
        return AgglomerativeClustering(linkage='ward', n_clusters=n_clusters)
    if algo == 'complete-linkage':
        return AgglomerativeClustering(linkage='complete', n_clusters=n_clusters)
    if algo == 'mini-k-means':
        return MiniBatchKMeans(n_clusters=n_clusters)
    if algo == 'k-means':
        return KMeans(n_clusters=n_clusters)



def generate_clustering_assignment_matrix(cluster_labels):
    mat = [[0 for x in range(len(cluster_labels))] for y in range(len(cluster_labels))]

    for h in range(0, len(cluster_labels)):
        for w in range(0, len(cluster_labels)):
            if cluster_labels[h] == cluster_labels[w]:
                mat[h][w] = 1

    return mat


def diff_with_initial_assignment(actual_sample_size, mat, new_clusters):
    l = len(mat)

    new_mat = [[0 for x in range(l)] for y in range(l)]

    for h in range(0, l):
        for w in range(0, l):
            if new_clusters[h] == new_clusters[w]:
                new_mat[h][w] = 1

    dif = 0
    for i in range(0, l):
        for j in range(0, l):
            if mat[i][j] != new_mat[i][j]:
                dif += 1

    return dif


def adjustment_with_cluster_number(algo, samples, actual_sample_size, n_clusers=None, mat=None):
    try:
        np_array = np.array(samples)
        model = select_proper_model(algo=algo, n_clusters=n_clusers)
        model.fit(np_array)

        if mat:
            return diff_with_initial_assignment(actual_sample_size, mat, model.labels_)
        else:
            return model
    except Exception as e:
        pass


def rebuild_model_with_adjustment(algo, samples, actual_sample_size, actual_labels, mat=None):
    n_clusers = 3

    cluster_delta = 1

    if mat:
        ref_diff, optimal_cluster = -1, -1

        while True:

            diff = adjustment_with_cluster_number(algo=algo, samples=samples, actual_sample_size=actual_sample_size,
                                                  n_clusers=n_clusers, mat=mat)
            if ref_diff == -1:
                ref_diff = diff
                optimal_cluster = n_clusers
            elif diff < ref_diff:
                optimal_cluster = n_clusers
                ref_diff = diff

            n_clusers = n_clusers + 1

            if n_clusers > 100:
                break



        model = adjustment_with_cluster_number(algo=algo, samples=samples, actual_sample_size=actual_sample_size,
                                               n_clusers=optimal_cluster, mat=None)


        return model

    else:
        model = None
        while True:
            model = adjustment_with_cluster_number(algo=algo, samples=samples, actual_sample_size=actual_sample_size,
                                                   n_clusers=n_clusers, mat=None)

            cluster_label_to_digit_dict = infer_cluster_labels(generated_labels=model.labels_,
                                                               actual_labels=actual_labels)
            predicted_labels = []
            for sample_index in range(0, len(model.labels_)):
                predicted_labels.append(cluster_label_to_digit_dict[model.labels_[sample_index]])

            correct_labels = 0
            for i in range(0, len(actual_labels)):
                if actual_labels[i] == predicted_labels[i]:
                    correct_labels += 1

            accuracy = correct_labels / len(predicted_labels)

            if accuracy > .6:
                break

            n_clusers = n_clusers + 1

            if n_clusers > 100:
                break

        return model


algo = 'single-linkage'

feature_map = {}

MAX_ALLOWED_CLUSTERS = 100

SAMPLE_SIZE = 200

# 2 %

MAX_ALLOWED_POINTS = 12

digit_combinations = [[0, 1, 6], [3, 4, 9]]

for combination in digit_combinations:
    print("Working with combination {}".format(combination))
    mat = None

    # INIT
    samples = []
    actual_labels = []
    for digit in combination:
        samples = samples + provide_random_p_samples(label=digit, num_of_samples=SAMPLE_SIZE, normalized=True)
        actual_labels = actual_labels + [digit] * SAMPLE_SIZE

    # real sample size INIT
    actual_sample_size = len(samples)

    # Mutate
    total_samples = samples.copy()
    label_of_samples = actual_labels.copy()

    adversarial_sample = []

    cluster_size_list = []

    original_cluster_labels = []
    original_cluster_number = None

    for sample_input in range(0, MAX_ALLOWED_POINTS + 1):
        print("sample input {}".format(sample_input))
        if sample_input == 0:
            _model_adjusted = rebuild_model_with_adjustment(algo=algo, samples=total_samples,
                                                            actual_sample_size=actual_sample_size,
                                                            actual_labels=label_of_samples)
            cluster_size_list.append((sample_input, _model_adjusted.n_clusters))
            mat = generate_clustering_assignment_matrix(_model_adjusted.labels_)
            original_cluster_labels = _model_adjusted.labels_.copy()
            original_cluster_number = _model_adjusted.n_clusters
            print("old clusters {}".format(_model_adjusted.n_clusters))


        else:
            _model_adjusted = rebuild_model_with_adjustment(algo=algo, samples=total_samples,
                                                            actual_sample_size=actual_sample_size,
                                                            actual_labels=label_of_samples, mat=mat)

            cluster_size_list.append((sample_input, _model_adjusted.n_clusters))
            print("new clusters {}".format(_model_adjusted.n_clusters))

            split, merge = get_split_merge(actual_sample_size,
                                           original_cluster_labels,
                                           _model_adjusted.labels_,
                                           prev_cluster_number=original_cluster_number,
                                           new_cluster_number=_model_adjusted.n_clusters)

            print("split, merge {}, {}".format(split, merge))

        adversarial_sample = get_adverserial_sample(_model_adjusted, total_samples, label_of_samples)


        total_samples.append(adversarial_sample)

        label_of_samples = label_of_samples + [-1]

    print(cluster_size_list)
