import json
import os

# result_director = "results"
from collections import defaultdict
from sklearn import metrics
import matplotlib.pyplot as plt
import numpy as np


digit_combinations = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]

sample_sizes = [100, 200, 300, 400, 500]


def get_files_from_path(result_director):
    file_name_Set = set()
    for file in os.listdir(result_director):
        file_name_Set.add(file)

    return file_name_Set


def evaluate_result_for_algo(algo):
    file_set = get_files_from_path()
    attribute_str_set = set()
    d = {}
    for file in file_set:
        if file.startswith(algo):
            delimited_attributes = file[len(algo) + 1:]
            last_pos = delimited_attributes.rfind("-")
            attributes_str = delimited_attributes[: last_pos]
            attribute_str_set.add(attributes_str)
            #SAMPLE_SIZE - COMB - N_CLUSTER

    for attribute_str in attribute_str_set:
        attributes = attribute_str.split("-")
        sample_size = attributes[0]
        combination_key = attributes[1]
        n_clusters = attributes[2]
        if combination_key not in d:
            d[combination_key] = set()
        d[combination_key].add((sample_size, n_clusters))

    for combination_key in d:
        size_cluster_tuple_list = [x for x in d[combination_key]]
        size_cluster_tuple_list.sort()

        size_cluster_result = []

        for (size, cluster) in size_cluster_tuple_list:
            # ALGO-SAMPLE_SIZE-1_2_3_4-N_CLUSTER
            reconstructed_key_prefix = algo + "-" + size + "-" + combination_key + "-" + cluster

            out_file = "results/{}-{}.json".format(reconstructed_key_prefix, 'predicted_labels')
            f = open(out_file, )
            predicted_labels = json.load(f)

            out_file = "results/{}-{}.json".format(reconstructed_key_prefix, 'actual_labels')
            f = open(out_file, )
            actual_labels = json.load(f)

            out_file = "results/{}-{}.json".format(reconstructed_key_prefix, 'time')
            f = open(out_file, )
            time = json.load(f)

            total_samples = len(predicted_labels)
            correctly_labelled = 0
            for index in range(0, total_samples):
                if predicted_labels[index] == actual_labels[index]:
                    correctly_labelled += 1

            accuracy = correctly_labelled / total_samples
            adjusted_rand_score = metrics.adjusted_rand_score(actual_labels, predicted_labels)
            adjusted_mutual_info_score = metrics.adjusted_mutual_info_score(actual_labels, predicted_labels)
            homogeneity_score = metrics.homogeneity_score(actual_labels, predicted_labels)
            completeness_score = metrics.completeness_score(actual_labels, predicted_labels)
            v_measure_score = metrics.v_measure_score(actual_labels, predicted_labels)


            res_d = {}

            res_d["accuracy"] = accuracy
            res_d["adjusted_rand_score"] = adjusted_rand_score
            res_d["adjusted_mutual_info_score"] = adjusted_mutual_info_score
            res_d["homogeneity_score"] = homogeneity_score
            res_d["completeness_score"] = completeness_score
            res_d["v_measure_score"] = v_measure_score

            size_cluster_result.append((size, cluster, res_d))

        import os
        path = "final_results/{}".format(algo)
        if not os.path.exists(path):
            os.makedirs(path)

        out_file = "{}/{}.json".format(path, combination_key)

        with open(out_file, "w") as ouf:
            json.dump(size_cluster_result, fp=ouf, indent=2)

CLUSTERING_ALGORITHMS = ['single-linkage', 'average-linkage', 'k-means', 'complete-linkage']

from tools.generate_combinations import generate as gen_combinations


def process_results():
    attributes = ["accuracy", "adjusted_rand_score", "adjusted_mutual_info_score",
                  "homogeneity_score", "completeness_score", "v_measure_score"]
    for samle_size in sample_sizes:
        dg = gen_combinations(10, 3)
        for combination in dg:
            comb_key = ""
            for digit in combination:
                if len(comb_key) == 0:
                    comb_key = comb_key + "-" + str(digit)
                else:
                    comb_key = comb_key + "_" + str(digit)

            for attribute in attributes:
                d = defaultdict(list)
                for algo in CLUSTERING_ALGORITHMS:
                    init_key = algo + "-" + str(samle_size)
                    init_key = init_key + comb_key

                    file_set = get_files_from_path("final_results/{}".format(algo))

                    for file in file_set:
                        if file.startswith(init_key):
                            out_file = "final_results/{}".format(algo) + "/" + file
                            f = open(out_file, )
                            attributes = json.load(f)
                            last_h = file.rfind("-")
                            cluster_size = int(file[last_h + 1: -5])
                            d[algo].append((cluster_size, attributes[attribute]))

                for key in d:
                    d[key].sort()
                d_len = [len(d[key]) for key in d]
                mn = min(d_len)
                if mn < 4:
                    continue

                ax = plt.subplot()
                for key in d:
                    x_ax = [p[0] for p in d[key]]
                    y_ax = [p[1] for p in d[key]]
                    plt.plot(x_ax, y_ax, marker='o',label=f"{key}")

                leg = plt.legend(loc='best',  ncol=2, fancybox=True)
                leg.get_frame().set_alpha(0.5)
                print("graphs/{}/{}-{}.png".format(attribute, comb_key[1:], samle_size))
                plt.savefig("graphs/{}/{}-{}.png".format(attribute, comb_key[1:], samle_size))
                plt.clf()




def evaluate_master_curve():
    # attr : {}
    attributes = ["accuracy", "adjusted_rand_score", "adjusted_mutual_info_score",
                  "homogeneity_score", "completeness_score", "v_measure_score"]

    for attribute in attributes:
        # (50, avg), (100, avg)
        dd = defaultdict(list)
        for samle_size in range(50, 550, 50):
            # put all instances of attribute under this sample size
            # against algorithm
            temp = defaultdict(list)
            for algo in CLUSTERING_ALGORITHMS:
                digit_combinations = gen_combinations(10, 3)
                for combination in digit_combinations:
                    comb_key = ""
                    for digit in combination:
                        if len(comb_key) == 0:
                            comb_key = comb_key + "-" + str(digit)
                        else:
                            comb_key = comb_key + "_" + str(digit)

                    init_key = algo + "-" + str(samle_size)
                    init_key = init_key + comb_key

                    file_set = get_files_from_path("final_results/{}".format(algo))

                    for file in file_set:
                        if file.startswith(init_key):
                            out_file = "final_results/{}".format(algo) + "/" + file
                            f = open(out_file, )
                            attributes = json.load(f)
                            last_h = file.rfind("-")
                            cluster_size = int(file[last_h + 1: -5])
                            temp[algo].append(attributes[attribute])
                            #d[algo].append((cluster_size, attributes[attribute]))

            for key in temp:
                dd[key].append((samle_size, sum(temp[key])/len(temp[key])))

            ax = plt.subplot()
            for key in dd:
                x_ax = [p[0] for p in dd[key]]
                y_ax = [p[1] for p in dd[key]]
                plt.plot(x_ax, y_ax, marker='o', label=f"{key}")

            plt.title("{} graph".format(attribute))
            plt.xlabel('Samples per digit')
            plt.ylabel('attribute')

            leg = plt.legend(loc='best', ncol=2, fancybox=True)
            leg.get_frame().set_alpha(0.5)
            plt.savefig("attribute_graph/{}.png".format(attribute))
            plt.clf()




def evaluate_three_d_curve():
    # attr : {}

    attributes = ["accuracy", "adjusted_rand_score", "adjusted_mutual_info_score",
                  "homogeneity_score", "completeness_score", "v_measure_score"]

    # for each algo: [sample_size][cluster_size] = attribute value ; name attribute_algo

    for attribute in attributes:
        # (50, avg), (100, avg)
        dd = defaultdict(list)
        d = defaultdict(list)
        for samle_size in range(50, 550, 50):
            # put all instances of attribute under this sample size
            # against algorithm
            temp = defaultdict(list)
            for algo in ['single-linkage']:
                digit_combinations = gen_combinations(10, 3)
                for combination in digit_combinations:
                    comb_key = ""
                    for digit in combination:
                        if len(comb_key) == 0:
                            comb_key = comb_key + "-" + str(digit)
                        else:
                            comb_key = comb_key + "_" + str(digit)

                    init_key = algo + "-" + str(samle_size)
                    init_key = init_key + comb_key

                    file_set = get_files_from_path("final_results/{}".format(algo))

                    for file in file_set:
                        if file.startswith(init_key):
                            out_file = "final_results/{}".format(algo) + "/" + file
                            f = open(out_file, )
                            attributes = json.load(f)
                            last_h = file.rfind("-")
                            cluster_size = int(file[last_h + 1: -5])
                            # temp[algo].append(attributes[attribute])
                            d[algo].append((samle_size, cluster_size, attributes[attribute]))


            for key in d:
                temp = d[key].copy()
                temp.sort()
                x_array = []
                y_array = []
                z_array = []

                for e in temp:
                    x_array.append(e[0])
                    y_array.append(e[1])
                    z_array.append(e[2])
                x_array = np.array(x_array)
                y_array = np.array(y_array)
                z_array = np.array(z_array)


                fig = plt.figure()
                ax1 = fig.add_subplot(111, projection='3d')

                ax1.bar3d(x_array,
                          y_array,
                          np.zeros(len(z_array)),
                          .2, .2, z_array)

                plt.show()



def tst_results():
    attributes = ["accuracy", "adjusted_rand_score", "adjusted_mutual_info_score",
                  "homogeneity_score", "completeness_score", "v_measure_score"]
    for samle_size in range(50, 550, 50):
        digit_combinations = gen_combinations(10, 3)
        for combination in digit_combinations:
            comb_key = ""
            for digit in combination:
                if len(comb_key) == 0:
                    comb_key = comb_key + "-" + str(digit)
                else:
                    comb_key = comb_key + "_" + str(digit)

            for attribute in attributes:
                d = defaultdict(list)
                for algo in CLUSTERING_ALGORITHMS:
                    init_key = algo + "-" + str(samle_size)
                    init_key = init_key + comb_key

                    file_set = get_files_from_path("final_results/{}".format(algo))

                    for file in file_set:
                        if file.startswith(init_key):
                            out_file = "final_results/{}".format(algo) + "/" + file
                            f = open(out_file, )
                            attributes = json.load(f)
                            last_h = file.rfind("-")
                            cluster_size = int(file[last_h + 1: -5])
                            d[algo].append((cluster_size, attributes[attribute]))

                slen = [(key, len(d[key])) for key in d]
                print(samle_size, comb_key, slen)







def evaluate_result():
    process_results()
    evaluate_master_curve()


evaluate_result()





